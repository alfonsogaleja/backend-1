package com.techu.backend.controller;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    //Endpoint default
    @GetMapping("")
    public String index(){
        return "API REST Tech U!";
    }

    //GET productos
    @GetMapping("/productos")
    public List<ProductoModel> getProductos(){
        return productoService.getProductos();
    }

    //GET a un único producto por ID
    @GetMapping("/productos/{id}")
    public ResponseEntity getProductosById(@PathVariable String id){
        ProductoModel pr = productoService.getProductoById(id);
        if(pr==null) {
            //no existe el producto
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    //GET subrecurso para obtener los usuarios de un producto por ID
    @GetMapping("/productos/{id}/usuarios")
    public String getUsersProductoById(@PathVariable String id){
        return "GET subrecurso usuarios del producto " + id;
    }

    //POST para crear un producto nuevo
    @PostMapping("/productos")
    public ResponseEntity<String> postProductos(@RequestBody ProductoModel nuevo){
        productoService.addProducto(nuevo);
        return new ResponseEntity<>("Producto creado.", HttpStatus.CREATED);
    }

    //PUT para actualizar un recurso
    @PutMapping("/productos/{id}")
    public ResponseEntity putProductoById(@PathVariable String id,
                                          @RequestBody ProductoModel productoModel){

        ProductoModel pr = productoService.getProductoById(id);
        if (pr==null){
            //no existe el producto
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }

        productoService.updateProductoById(id, productoModel);
        return new ResponseEntity<>("Producto actualizado correctamente.",HttpStatus.OK);
    }

    @GetMapping("/productos/{id}/users")
    public ResponseEntity getUsers(@PathVariable String id){
        ProductoModel pr = productoService.getProductoById(id);
        if(pr==null){
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }

        if (pr.getUsers()!=null){
            return ResponseEntity.ok(pr.getUsers());
        }else{
            return new ResponseEntity<>("Producto sin usuarios", HttpStatus.NO_CONTENT); // 204 No Content

        }
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProductoById(@PathVariable String id){

        ProductoModel pr = productoService.getProductoById(id);
        if (pr==null){
            //no existe el producto
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }

        productoService.removeProductoById(id);
        return new ResponseEntity<>("Producto eliminado correctamente.",HttpStatus.NO_CONTENT); // 204 No Content
    }

    @PatchMapping("/productos/{id}")
    public ResponseEntity patchProductoById(@PathVariable String id,
                                            @PathVariable double precio){
        ProductoModel pr = productoService.getProductoById(id);
        if (pr==null){
            //no existe el producto
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoService.updateProductoPrecioById(id, precio);
        return new ResponseEntity<>("Producto actualizado correctamente.",HttpStatus.NO_CONTENT); // 204 No Content

    }
}
