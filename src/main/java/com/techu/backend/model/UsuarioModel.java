package com.techu.backend.model;

public class UsuarioModel {
    private String id;
    private String nombre;
    private String rol;

    public UsuarioModel (String id){
        this.id = id;
    }

    public UsuarioModel(String id, String nombre, String rol) {
        this.id = id;
        this.nombre = nombre;
        this.rol = rol;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
}
