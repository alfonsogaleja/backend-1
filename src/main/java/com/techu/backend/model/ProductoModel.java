package com.techu.backend.model;

import java.util.List;

public class ProductoModel {

    private String id;
    private String descripcion;
    private ProductoPrecioModel precio;
    private List<UsuarioModel> users;

    public ProductoModel(String id, String descripcion, double precio) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = new ProductoPrecioModel(id, precio);
        this.users = null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ProductoPrecioModel getPrecio() {
        return precio;
    }

    public void setPrecio(ProductoPrecioModel precio) {
        this.precio = precio;
    }

    public List<UsuarioModel> getUsers() {
        return users;
    }

    public void setUsers(List<UsuarioModel> users) {
        this.users = users;
    }
}
