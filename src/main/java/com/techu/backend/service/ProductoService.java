package com.techu.backend.service;

import com.techu.backend.model.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductoService {

    private List<ProductoModel> productoList = new ArrayList<>();

    public ProductoService() {


        List<UsuarioModel> users = new ArrayList<>();
        users.add(new UsuarioModel("1"));
        users.add(new UsuarioModel("2"));
        users.add(new UsuarioModel("3"));
        users.add(new UsuarioModel("4"));

        productoList.add(new ProductoModel("1","producto 1",100.10));
        productoList.add(new ProductoModel("2","producto 2",100.11));
        productoList.add(new ProductoModel("3","producto 3",100.12));
        productoList.add(new ProductoModel("4","producto 4",100.13));
        productoList.add(new ProductoModel("5","producto 5",100.14));

        //asociamos usuarios a algunos productos
        productoList.get(1).setUsers(users);
        productoList.get(2).setUsers(users);

    }

    // READ productos
    public List<ProductoModel> getProductos(){
        return productoList;
    }

    // READ producto por id
    public ProductoModel getProductoById(String id){
        // busqueda mediante un bucle
        int pos=getIndex(id);
        if (pos>=0) {
            return productoList.get(pos);
        }
        return null;
    }

    // CREATE nuevo producto y añadirlo a la lista
    public ProductoModel addProducto(ProductoModel nuevo){
        productoList.add(nuevo);
        return nuevo;
    }

    // Update
    public ProductoModel updateProductoById(String id, ProductoModel nuevoProducto){
        int pos=getIndex(id);
        if(pos>=0){
            productoList.set(pos, nuevoProducto);
            return productoList.get(pos);
        }
        return null;
    }

    // Delete
    public void removeProductoById(String id){
        int pos=getIndex(id);
        if(pos>=0) {
            productoList.remove(pos);
        }
    }

    public ProductoModel updateProductoPrecioById(String id,
                                                  double newPrecio){
        int pos = getIndex(id);
        if (pos >= 0) {
            ProductoModel pr = productoList.get(pos);
            ProductoPrecioModel ppm = pr.getPrecio();
            ppm.setPrecio(newPrecio);
            return pr;
        }
        return null;
    }

    // Devuelve la posición de un producto en una lista de producto
    public int getIndex(String id){
        int pos=0;
        while(pos<productoList.size()){
            if(id.equals(productoList.get(pos).getId())){
                return(pos);
            }
            pos++;
        }
        return -1;
    }

}
